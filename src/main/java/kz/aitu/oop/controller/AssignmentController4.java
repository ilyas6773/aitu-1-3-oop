package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import static java.lang.Integer.parseInt;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {
    public static void main(String args[]) throws IOException {

        AssignmentController4 assignmentController4 = new AssignmentController4();
        System.out.println(assignmentController4.getStudentsByGroup("IT1901"));
        System.out.println(assignmentController4.getGroupStats("IT1901"));
        System.out.println(assignmentController4.getTopStudents());
    }

    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "\n";
        StudentFileRepository StudFile = new StudentFileRepository();
        int groupnum = parseInt(group.substring(2));
        for(Student stu: StudFile.getStudents()){
            if(parseInt(stu.getGroup().substring(2))==groupnum){
                result += stu.getName()+"\n";
            }
        }

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "\n";
        StudentFileRepository StudFile = new StudentFileRepository();
        int A=0,B=0,C=0,D=0,F=0;
        for(Student stu: StudFile.getStudents()){
            if(stu.getPoint()>89){
                A++;
            }
            if(stu.getPoint()<90&& stu.getPoint()>74){
                B++;
            }
            if(stu.getPoint()<75&& stu.getPoint()>59){
                C++;
            }
            if(stu.getPoint()<60&& stu.getPoint()>49){
                D++;
            }
            if(stu.getPoint()<50){
                F++;
            }
        }
        result+="A-"+A+", "+"B-"+B+", "+"C-"+C+", "+"D-"+D+", "+"F-"+F+"\n";
        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here
        String result = "\n";
        StudentFileRepository StudFile = new StudentFileRepository();

        ArrayList<String> Names=new ArrayList<>();
        ArrayList<Double> Points=new ArrayList<>();
        for(Student stu: StudFile.getStudents()){
            Names.add(stu.getName());
            Points.add(stu.getPoint());
        }
        double temp = 0;
        String tempo="";
        for(int i=0;i<Names.size()-1;i++){
            for(int j=0;j<Names.size()-1-i;j++){
                if(Points.get(j)>Points.get(j+1)){
                    temp=Points.get(i);
                    Points.set(i,Points.get(i+1));
                    Points.set(i+1,temp);

                    tempo=Names.get(i);
                    Names.set(i,Names.get(i+1));
                    Names.set(i+1,tempo);
                }
            }
        }

        for(int i=0;i<5;i++){
            result+= Names.get(i)+"\n";
        }
        return ResponseEntity.ok(result);
    }
}
