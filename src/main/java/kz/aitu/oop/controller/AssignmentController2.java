package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static java.lang.Integer.parseInt;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result +=student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "\t"+ student.getGroup()+"</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {
    //////////////////////////////////////////
        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        double count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {
        double counter=0;
        ///////////////////////////////////////////////
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        List<Student> students = studentFileRepository.getStudents();

        String currentGroup = students.get(0).getGroup();
        double temp = 0;
        List<Double> ltemp = new ArrayList<>();

        for (int i = 0;i<students.size();i++){
            Student student = students.get(i);
            if (student.getGroup().equals(currentGroup)){
                temp++;
            } else {
                currentGroup=student.getGroup();
                ltemp.add(temp);
                temp=1;
            }
            if (i==students.size()-1){
                ltemp.add(temp);
            }
        }

        for (int i=0;i<ltemp.size();i++){
            counter+=ltemp.get(i);
        }
        counter/=ltemp.size();

        return ResponseEntity.ok(counter);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {
        //////////////////////////////
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double average = 0;
        //change your code here
        double total = 0;
        double count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getPoint();
            count++;
        }
        average = total / count;


        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {
        ////////////////////////////////////
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double average = 0;
        //change your code here
        double total = 0;
        double count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getAge();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {
        //////////////////////////////////////////////
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxPoint = 0;
        //change your code here
        double check = 0;
        for (Student student: studentFileRepository.getStudents()) {
            check = student.getPoint();
            if(check>maxPoint){
                maxPoint=check;
            }
        }
        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {
        ///////////////////////////////////////////
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxAge = 0;
        //change your code here
        double check = 0;
        for (Student student: studentFileRepository.getStudents()) {
            check = student.getAge();
            if(check>maxAge){
                maxAge=check;
            }
        }
        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        List<Student> students = studentFileRepository.getStudents();

        String currentGroup = students.get(0).getGroup();
        double temp = 0;
        int count=0;
        List<Double> ltemp = new ArrayList<>();

        for (int i = 0;i<students.size();i++){
            Student student = students.get(i);
            if (student.getGroup().equals(currentGroup)){
                temp+=student.getPoint();
                count++;
            } else {
                currentGroup=student.getGroup();
                temp/=count;
                ltemp.add(temp);
                temp=student.getPoint();
                count=1;
            }
        }

        averageGroupPoint = ltemp.get(0);

        for (int i=1;i<ltemp.size();i++){
            if (ltemp.get(i)>averageGroupPoint){
                averageGroupPoint=ltemp.get(i);
            }
        }

        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {
        double averageGroupAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        List<Student> students = studentFileRepository.getStudents();

        String currentGroup = students.get(0).getGroup();
        double temp = 0;
        int count=0;
        List<Double> ltemp = new ArrayList<>();

        for (int i = 0;i<students.size();i++){
            Student student = students.get(i);
            if (student.getGroup().equals(currentGroup)){
                temp+=student.getAge();
                count++;
            } else {
                currentGroup=student.getGroup();
                temp/=count;
                ltemp.add(temp);
                temp=student.getAge();
                count=1;
            }
        }

        averageGroupAge = ltemp.get(0);

        for (int i=1;i<ltemp.size();i++){
            if (ltemp.get(i)>averageGroupAge){
                averageGroupAge=ltemp.get(i);
            }
        }


        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
