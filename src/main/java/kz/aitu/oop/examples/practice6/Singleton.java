package kz.aitu.oop.examples.practice6;

public class Singleton {
    private static Singleton instance = null;

    public String str;

    private Singleton(){
        str = "Hello I am a singleton! Let me say hello world to you.";
    }

    public static Singleton getInstance(){
        if (instance == null){
            instance = new Singleton();
        }
        return instance;
    }
}
