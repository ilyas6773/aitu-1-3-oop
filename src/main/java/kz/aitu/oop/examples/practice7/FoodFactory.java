package kz.aitu.oop.examples.practice7;

public class FoodFactory {
    public Object getFood(String str){
        if (str.equals("cake") || str.equals("Cake")){
            Cake cake = new Cake();
            cake.getType();
            return cake;
        }
        if (str.equals("pizza") || str.equals("Pizza")){
            Pizza pizza = new Pizza();
            pizza.getType();
            return pizza;
        }
        return null;
    }
}
