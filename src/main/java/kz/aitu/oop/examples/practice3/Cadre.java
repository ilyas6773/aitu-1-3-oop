package kz.aitu.oop.examples.practice3;

public class Cadre extends Employee {
    private int id;
    private String education;
    private String exp;
    public Cadre(String dep, String name, int id, String education, String exp) {
        super(dep, name);
        this.id=id;
        this.education=education;
        this.exp=exp;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public int getId() {
        return id;
    }

    public String getEducation() {
        return education;
    }

    public String getExp() {
        return exp;
    }

    @Override
    public String toString() {
        return "Cadre{" +
                "id=" + id +
                ", education='" + education + '\'' +
                ", exp='" + exp + '\'' +
                '}';
    }
}
