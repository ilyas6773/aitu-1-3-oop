package kz.aitu.oop.examples.practice3;

public class Employee {
    private String name;
    private String dep;
    public Employee(String dep, String name){
        this.name = name;
        this.dep=dep;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getName() {
        return name;
    }

    public String getDep() {
        return dep;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", dep='" + dep + '\'' +
                '}';
    }
}

