package kz.aitu.oop.examples.practice3;

public class Project extends Department {
    private String proj;
    private int cost;
    public Project(String dep, String name, String title, int amount, String proj, int cost) {
        super(dep, name, title, amount);
        this.cost=cost;
        this.proj=proj;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setProj(String proj) {
        this.proj = proj;
    }

    public int getCost() {
        return cost;
    }

    public String getProj() {
        return proj;
    }

    @Override
    public String toString() {
        return "Project{" +
                "proj='" + proj + '\'' +
                ", cost=" + cost +
                '}';
    }
}
