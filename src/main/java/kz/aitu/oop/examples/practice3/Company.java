package kz.aitu.oop.examples.practice3;

public class Company extends Employee {
    private String designation;
    private String type;
    public Company(String dep, String name, String designation, String type) {
        super(dep, name);
        this.designation = designation;
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getType() {
        return type;
    }

    public String getDesignation() {
        return designation;
    }

    @Override
    public String toString() {
        return "Company{" +
                "designation='" + designation + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
