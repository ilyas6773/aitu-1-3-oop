package kz.aitu.oop.examples.practice3;

public class Department extends Employee{
    private String title;
    private int amount;

    public Department(String dep, String name, String title, int amount) {
        super(dep, name);
        this.title=title;
        this.amount = amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Department{" +
                "title='" + title + '\'' +
                ", amount=" + amount +
                '}';
    }
}
