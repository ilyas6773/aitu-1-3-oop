package kz.aitu.oop.examples.practice4;

public class Aquarium {
    private String title;
    public Aquarium(String title){
        this.title=title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Aquarium{" +
                "title='" + title + '\'' +
                '}';
    }

}
