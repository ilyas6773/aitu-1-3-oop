package kz.aitu.oop.examples.practice4;

public class Construction extends Aquarium {
    private String name;
    public Construction(String title) {
        super(title);
        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Construction{" +
                "name='" + name + '\'' +
                '}';
    }
}
