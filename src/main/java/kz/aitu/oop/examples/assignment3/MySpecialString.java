package kz.aitu.oop.examples.assignment3;

public class MySpecialString {
    // keep the array values internally without duplicated values
    int[] valuas;
    public MySpecialString(int[] values){
        this.valuas=values;
    }
    // return the number of values that are stored
    public int length(){
        return valuas.length;
    }
    // return the value stored at position or -1 if position is not available
    public int valueAt(int position){
        if(valuas[position] == '\0'){
            return 0;
        }
        return valuas[position];
    }
    // return true if value is stored, otherwise false
    public boolean contains(int value){
        for(int i=0; i < valuas.length; i++){
            if(valuas[i] == value){
                return true;
            }
        }
        return false;
    }
    // count for how many time value is stored
    public int count(int value){
        int counter = 0;
        for(int i=0; i < valuas.length; i++){
            if(valuas[i] == value){
                counter++;
            }
        }
        return counter;
    }
//print the stored values ...
    public void print(){
        for(int i=0; i < valuas.length; i++){
            System.out.println(valuas[i]);
        }
    }

}
