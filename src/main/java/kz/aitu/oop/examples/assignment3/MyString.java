package kz.aitu.oop.examples.assignment3;

public class MyString {
    int[] values;

    public MyString(int[] values){
        this.values=values;
    }
    public int length(){
        return values.length;
    }

    public int valueAt(int position){
        if(values[position] == '\0'){
            return 0;
        }
        return values[position];
    }
    public boolean contains(int value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                return true;
            }
        }
        return false;
    }
    public int count(int value){
        int counter = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                counter++;
            }
        }
        return counter;
    }
    public void print(){
        for(int i=0; i < values.length; i++){
            System.out.println(values[i]);
        }
    }

    public boolean check(MyString x, MyString y){
        for(int i=0;i<x.length();i++){
            if(x.valueAt(i)!=y.valueAt(i)){
                return false;
            }
        }
        return true;
    }

}
