package kz.aitu.oop.examples.MovObj;

public interface Movable {
     void moveUp();
     void moveDown();
     void moveLeft();
     void moveRight();
}
