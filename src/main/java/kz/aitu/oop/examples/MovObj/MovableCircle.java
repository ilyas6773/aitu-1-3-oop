package kz.aitu.oop.examples.MovObj;

public class MovableCircle implements Movable {
    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed,int radius){
        this.radius=radius;
        center.x=x;
        center.y=y;
        center.ySpeed=ySpeed;
        center.xSpeed=xSpeed;
    }

    public String toString(){
        return "";
    }

    @Override
    public void moveUp() {

    }

    @Override
    public void moveDown() {

    }

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveRight() {

    }
}
