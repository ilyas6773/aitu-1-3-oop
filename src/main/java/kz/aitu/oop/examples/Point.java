package kz.aitu.oop.examples;
import java.lang.Math;

public class Point {
    private double x;
    private double y;

    public Point(double x,double y){
        this.x = x;
        this.y = y;
    }

    public void setPoint(double x,double y){
        this.x = x;
        this.y = y;
    }

    public double[] getPoint(){
        double[] ans = new double[2];
        ans[0] = this.x;
        ans[1] = this.y;
        return ans;
    }

    public double Distance(Point p){
        double[] points = p.getPoint();
        double d = Math.sqrt(
                Math.pow(this.x-points[0], 2)+Math.pow(this.y-points[1], 2)
        );
        return d;
    }
}
