package kz.aitu.oop.examples.practice5;

public class Value extends Gemstone {
    private int worth;
    public Value(String name, int weight, int worth) {
        super(name, weight);
        this.worth=worth;
    }

    public void setWorth(int worth) {
        this.worth = worth;
    }

    public int getWorth() {
        return worth;
    }

    @Override
    public String toString() {
        return "Value{" +
                "worth=" + worth +
                '}';
    }
}
