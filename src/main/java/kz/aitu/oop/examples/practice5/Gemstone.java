package kz.aitu.oop.examples.practice5;

public class Gemstone {
    private String name;
    private int weight;
    public Gemstone(String name, int weight){
        this.name=name;
        this.weight=weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Gemstone{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
