package kz.aitu.oop.examples.practice5;

public class Necklace extends Gemstone {
    private int numSlots;
    public Necklace(String name, int weight) {
        super(name, weight);
    }

    public void setNumSlots(int numSlots) {
        this.numSlots = numSlots;
    }

    public int getNumSlots() {
        return numSlots;
    }

    @Override
    public String toString() {
        return "Necklace{" +
                "numSlots=" + numSlots +
                '}';
    }
}
