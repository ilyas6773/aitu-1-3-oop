package kz.aitu.oop.examples.practice5;

public class Subfamily  extends Gemstone{
    private String subFam;

    public Subfamily(String name, int weight, String subFam) {
        super(name, weight);
        this.subFam=subFam;
    }

    public void setSubFam(String subFam) {
        this.subFam = subFam;
    }

    public String getSubFam() {
        return subFam;
    }


}
