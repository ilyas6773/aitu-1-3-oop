package kz.aitu.oop.examples.practice1;

public class Main {
    public static void main(String[] args) throws Exception {
        try {
            throw new Exception("Close the blast-door");
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            System.out.println("Open the blast-door");
        }
    }
}