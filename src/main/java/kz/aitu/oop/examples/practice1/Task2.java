package kz.aitu.oop.examples.practice1;

import java.io.File;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

    }

    public static boolean fileExists(String path){
        try {
            File f = new File(path);
            Scanner sc = new Scanner(f);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public static boolean isInt(String text){
        try {
            for(int i=0; i<text.length();i++){
                char c = text.charAt(i);
                if(47<c || c>58){
                    throw new Exception("Close the blast-door");
                }
            }
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public static boolean isDouble(String text){
        try {
            for(int i=0; i<text.length();i++){
                char c = text.charAt(i);
                if(47<c || c>58 || c!=46){
                    throw new Exception("Close the blast-door");
                }
            }
        } catch (Exception e){
            return false;
        }
        return true;
    }
}
