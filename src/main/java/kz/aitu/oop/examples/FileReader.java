package kz.aitu.oop.examples;

// Java Program to illustrate reading from Text File
// using Scanner Class
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) throws Exception {

        FileReader fileReader = new FileReader();

        System.out.println(fileReader.getFile("C:\\Users\\miste\\Desktop\\input.txt"));
    }


    public String getFile(String path) throws FileNotFoundException {

        File file = new File(path);
        Scanner sc = new Scanner(file);

        StringBuilder result = new StringBuilder();
        while (sc.hasNextLine())
            result.append(sc.nextLine()).append("\n");


        return result.toString();
    }
}
