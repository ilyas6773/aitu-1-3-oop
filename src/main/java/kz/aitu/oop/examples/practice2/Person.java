package kz.aitu.oop.examples.practice2;

public class Person extends Train{
    private int age;
    private String forename;
    public Person(String name, String type, int volume, int age, String forename) {
        super(name, type, volume);
        this.age = age;
        this.forename = forename;
    }

    public Person() {
        super();
        this.age = age;
        this.forename = forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getForename() {
        return forename;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", forename='" + forename + '\'' +
                '}';
    }
}
