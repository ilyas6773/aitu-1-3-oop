package kz.aitu.oop.examples.practice2;

public class Passenger extends Person{
    private String place;

    public Passenger(String name, String type, int volume, int age, String forename, String place) {
        super(name, type, volume, age, forename);
        this.place=place;
    }

    public Passenger() {
        super();
        this.place=place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPlace() {
        return place;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "place='" + place + '\'' +
                '}';
    }
}
