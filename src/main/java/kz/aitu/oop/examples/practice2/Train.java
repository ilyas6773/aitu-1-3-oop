package kz.aitu.oop.examples.practice2;

public class Train {
    private String name;
    private String type;
    private int volume;

    public Train(){
        this.name = name;
        this.type = type;
        this.volume = volume;
    }

    public Train(String name, String type, int volume){
        this.name = name;
        this.type = type;
        this.volume = volume;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    public String toString(){
        return "";
    }
}
