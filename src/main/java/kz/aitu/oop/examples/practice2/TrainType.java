package kz.aitu.oop.examples.practice2;

public class TrainType extends Train {
    private String style;
    public TrainType(String name, String type, int volume, String style) {
        super(name, type, volume);
        this.style = style;
    }

    public TrainType() {
        super();
        this.style = style;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return "TrainType{" +
                "style='" + style + '\'' +
                '}';
    }
}
