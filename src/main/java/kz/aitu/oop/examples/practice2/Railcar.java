package kz.aitu.oop.examples.practice2;

public class Railcar extends Train{
    private int capacity;
    private int numPass;

    public Railcar(String name, String type, int volume, int capacity, int numPass) {
        super(name, type, volume);
        this.capacity=capacity;
        this.numPass=numPass;
    }

    public Railcar() {
        super();
        this.capacity=capacity;
        this.numPass=numPass;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setNumPass(int numPass) {
        this.numPass = numPass;
    }

    public int getNumPass() {
        return numPass;
    }

    @Override
    public String toString() {
        return "Railcar{" +
                "capacity=" + capacity +
                ", numPass=" + numPass +
                '}';
    }
}
