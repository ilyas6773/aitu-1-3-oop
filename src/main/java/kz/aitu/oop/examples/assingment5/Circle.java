package kz.aitu.oop.examples.assingment5;

import lombok.Data;

import java.io.IOException;

public class Circle extends Shape{

    private double radius;

    public Circle(){
        this.radius = 1.0;
    }

    public Circle(double rad){
        this.radius=rad;
    }
    public Circle(double rad, String clr, boolean fill){
        this.radius=rad;
        setColor(clr);
        setFilled(fill);
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double rad){
        this.radius = rad;
    }

    @Override
    public String toString() {
        return "A Circle with radius="+radius+", which is a subclass of "+ super.toString();
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius,2);
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }
}
