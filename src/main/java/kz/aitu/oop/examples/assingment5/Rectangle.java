package kz.aitu.oop.examples.assingment5;

public class Rectangle extends Shape{
    private double width;
    private double length;

    public Rectangle(){
        this.width =1.0;
        this.length=1.0;
    }

    public Rectangle(double wid, double len){
        this.width=wid;
        this.length=len;
    }

    public Rectangle(double wid, double len, String clr, boolean fill){
        this.width=wid;
        this.length=len;
        setColor(clr);
        setFilled(fill);
    }

    public double getLength(){
        return length;
    }

    public void setLength(double len){
        this.length = len;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double wid){
        this.width = wid;
    }

    @Override
    public String toString() {
        return "A Rectangle with width="+width+" and length="+length+" which is a subclass of "+ super.toString();
    }

    @Override
    public double getArea() {
        return length*width;
    }

    @Override
    public double getPerimeter() {
        return 2*(length+width);
    }
}
