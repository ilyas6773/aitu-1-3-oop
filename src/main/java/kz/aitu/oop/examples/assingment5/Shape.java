package kz.aitu.oop.examples.assingment5;

import kz.aitu.oop.controller.AssignmentController4;

import java.io.IOException;

public abstract class Shape {
    public static void main(String args[]) throws IOException {

    }
    protected String color;
    protected boolean filled;

    public Shape(){
        this.color="green";
        this.filled=true;
    }

    public Shape(String colr, boolean tilt){
        this.color = colr;
        this.filled = tilt;
    }

    public boolean isFilled(){
        return filled;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String clr){
        this.color=clr;
    }

    public void setFilled(boolean filled){
        this.filled=filled;
    }

    public String toString(){
        String s = "";
        if(filled ==true){
            s = "filled";}
        else{s="not filled";}
        return "A shape with color "+color+" and "+s;
    }
    //public abstract double getRadius();
    public abstract double getArea();
    public abstract double getPerimeter();
}
