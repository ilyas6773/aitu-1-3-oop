package kz.aitu.oop.examples.assingment5;

import java.io.IOException;

public class Square extends Rectangle{


    public Square(double side1){
        super(side1,side1);
    }

    public Square(double s, String clr, boolean fill) {
        super(s, s,clr,fill);
    }

    public double getSide(){
        return super.getLength();
    }

    public void setSide(double s){
        super.setLength(s);
        super.setWidth(s);
    }
    //////////////////////////////////////////////////
    public double getPerimeter(){
        return super.getPerimeter();
    }

    public double getArea(){
        return super.getArea();
    }

    @Override
    public String toString() {
        return "A Square with side="+getSide()+", which is a subclass of "+super.toString();
    }
}
