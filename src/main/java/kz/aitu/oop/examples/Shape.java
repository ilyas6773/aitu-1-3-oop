package kz.aitu.oop.examples;
/*
5.4051411340736175
8.48528137423857
59.45655247480979
*/

import java.util.List;

public class Shape {
    private List<Point> points;

    public void setPoints(List<Point> ps){
        points = ps;
    }

    public List<Point> getPoints(){
        return points;
    }

    public void addPoint(Point p){
        points.add(p);
    }

    public double getPerimeter(){
        double ans=0;
        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            ans+=p1.Distance(p2);
        }
        Point pstart = points.get(0);
        Point pend = points.get(points.size()-1);
        ans+=pstart.Distance(pend);
        return ans;
    }

    public double calcLongestSide(){
        double ans = 0;
        Point pa = points.get(0);
        Point pb = points.get(1);
        double check =0;
        check += pa.Distance(pb);
        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            ans=p1.Distance(p2);
            if(check<ans){
                check = ans;
            }
        }
        return check;
    }

    public double calcAverageLen(){
        double ans = 0;


        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            ans +=p1.Distance(p2);
        }

        Point pst = points.get(0);
        Point pend = points.get(points.size()-1);
        ans += pst.Distance(pend);


        ans /= points.size()+1;
        return ans;
    }
}
