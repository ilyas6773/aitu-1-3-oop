package kz.aitu.oop.examples.GeoObj;

public class ResizeableCircle extends Circle implements Resizable{
    public ResizeableCircle(double radius) {
        super(radius);
    }

    public String toString(){
        return "Circle with radius = "+radius+" Perimeter = "+getPerimeter()+" and Area = "+getArea();
    }

    @Override
    public void resize(int percent) {
        this.radius *= percent;
    }
}
