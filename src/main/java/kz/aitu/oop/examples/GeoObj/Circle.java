package kz.aitu.oop.examples.GeoObj;

public class Circle implements GeometricObject {

    protected double radius;

    public Circle(double radius){
        this.radius=radius;
    }

    public String toString(){
        return "Circle with radius = "+radius+" Perimeter = "+getPerimeter()+" and Area = "+getArea();
    }

    @Override
    public double getPerimeter() {
        return Math.PI * radius*2;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius,2);
    }
}
