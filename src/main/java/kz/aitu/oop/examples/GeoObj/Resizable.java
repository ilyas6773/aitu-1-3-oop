package kz.aitu.oop.examples.GeoObj;

public interface Resizable {
    void resize(int percent);
}
