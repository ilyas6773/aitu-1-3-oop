package kz.aitu.oop.examples.GeoObj;

public interface GeometricObject {
    double getPerimeter();
    double getArea();
}
